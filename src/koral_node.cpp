#include "koralROS/Detector.h"
#include "koralROS/Matcher.h"
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <getopt.h>
#include "xxhash.h"

#include <iostream>
#include <pqxx/pqxx>

class koralROS
{
public:
	koralROS(FeatureDetector &detector_, FeatureMatcher &matcher_, std::string s1topic, std::string s2topic) : detector(detector_), matcher(matcher_)
	{
		imageL_sub.subscribe(node, s1topic, 1);
		imageR_sub.subscribe(node, s2topic, 1);
		imageSync.reset(new ImageSync(LRSyncPolicy(10), imageL_sub, imageR_sub));
		imageSync->registerCallback(boost::bind(&koralROS::imageCallback, this, _1, _2));
	}

	void imageCallback(const sensor_msgs::ImageConstPtr &img1, const sensor_msgs::ImageConstPtr &img2)
	{
		cv_bridge::CvImagePtr imagePtr1, imagePtr2;
		imagePtr1 = cv_bridge::toCvCopy(img1, sensor_msgs::image_encodings::MONO8);
		std::cout << "Left camera: ";
		detector.extractFeatures(imagePtr1);
		matcher.setTrainingImage(detector.kps, detector.desc);
		cv::drawKeypoints(imagePtr1->image, detector.converted_kps, image_with_kps_L, cv::Scalar::all(-1.0), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
		kpsL = detector.converted_kps;

		imagePtr2 = cv_bridge::toCvCopy(img2, sensor_msgs::image_encodings::MONO8);
		std::cout << "Right camera: ";
		detector.extractFeatures(imagePtr2);
		matcher.setQueryImage(detector.kps, detector.desc);
		cv::drawKeypoints(imagePtr2->image, detector.converted_kps, image_with_kps_R, cv::Scalar::all(-1.0), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
		kpsR = detector.converted_kps;
		detector.receivedImg = true;
	}

	cv::Mat image_with_kps_L, image_with_kps_R;
	std::vector<cv::KeyPoint> kpsL, kpsR;

private:
	ros::NodeHandle node;

	FeatureDetector &detector;
	FeatureMatcher &matcher;
	message_filters::Subscriber<sensor_msgs::Image> imageL_sub, imageR_sub;

	typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> LRSyncPolicy;

	typedef message_filters::Synchronizer<LRSyncPolicy> ImageSync;
	boost::shared_ptr<ImageSync> imageSync;
};

int main(int argc, char **argv)
{
	ros::init(argc, argv, "koralROS"); 
	ros::NodeHandle nh;

	int opt = 0;
	unsigned int width = 640;
	unsigned int height = 480;
	unsigned int maxFeatureCount = 50000;
	uint8_t fastThreshold = 40;
	uint8_t matchThreshold = 25;

	unsigned int scaleLevels = 8;
	float scaleFactor = 1.2;

	std::string sense1Topic = "imageL"; 
	std::string sense2Topic = "imageR";

	const char *opts = "+"; // set "posixly-correct" mode
	const option longopts[]{
		{"width", 1, 0, '0'},
		{"height", 1, 0, '1'},
		{"maxFeatureCount", 1, 0, '2'},
		{"fastThreshold", 1, 0, '3'},
		{"matchThreshold", 1, 0, '4'},
		{"scaleLevels", 1, 0, '5'},
		{"scaleFactor", 1, 0, '6'},
		{"sense1Topic", 1, 0, '7'},
		{"sense2Topic", 1, 0, '8'},
		{0, 0, 0, 0}}; 

	while ((opt = getopt_long_only(argc, argv, opts, longopts, 0)) != -1)
	{
		switch (opt)
		{
		case '3':
			std::cout << "setting fastThreshold: " << optarg << std::endl;
			fastThreshold = atoi(optarg);
			break;
		case '4':
			std::cout << "setting matchThreshold: " << optarg << std::endl;
			matchThreshold = atoi(optarg);
			break;
		case '2':
			std::cout << "setting macFeatureCount: " << optarg << std::endl;
			maxFeatureCount = atoi(optarg);
			break;
		case '5':
			std::cout << "setting scaleLevels: " << optarg << std::endl;
			scaleLevels = atoi(optarg);
			break;
		case '6':
			std::cout << "setting scaleFactor: " << optarg << std::endl;
			scaleFactor = atof(optarg);
			break;
		case '0':
			std::cout << "setting width: " << optarg << std::endl;
			width = atoi(optarg);
			break;
		case '1':
			std::cout << "setting height: " << optarg << std::endl;
			height = atoi(optarg);
			break;
		case '7':
			std::cout << "setting sense1Topic: " << optarg << std::endl;
			sense1Topic = optarg;
			break;
		case '8':
			std::cout << "setting sense2Topic: " << optarg << std::endl;
			sense2Topic = optarg;
			break;
		default: /* '?' */
			exit(EXIT_FAILURE);
		}
	}

	FeatureDetector detector(scaleFactor, scaleLevels, width, height, maxFeatureCount, fastThreshold);
	FeatureMatcher matcher(matchThreshold, maxFeatureCount);
	koralROS koral(detector, matcher, sense1Topic, sense2Topic);

	//image_transport::Subscriber sub1 = it.subscribe("imageL", 1, &FeatureDetector::imageCallbackL, &detector);
	//image_transport::Subscriber sub2 = it.subscribe("imageR", 1, &FeatureDetector::imageCallbackR, &detector);

	pqxx::connection c("postgresql://postgres@localhost/postgres");
  	pqxx::work txn(c);

	while (ros::ok())
	{
		ros::spinOnce();
		if (detector.receivedImg)
		{
			matcher.matchFeatures();
			cv::Mat image_with_matches;
			detector.converted_kps.clear();
			cv::drawMatches(koral.image_with_kps_L, koral.kpsL, koral.image_with_kps_R, koral.kpsR, matcher.dmatches, image_with_matches, cv::Scalar::all(-1.0), cv::Scalar::all(-1.0), std::vector<char>(), cv::DrawMatchesFlags::DEFAULT);
			cv::namedWindow("Matches", CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO);
			cv::imshow("Matches", image_with_matches);
			cv::waitKey(1);
			detector.receivedImg = false;
		}
	}

	detector.freeGPUMemory();
	matcher.freeGPUMemory();
	cudaDeviceReset();
	return 0;
}
