#!/bin/bash
set -e
./build/devel/lib/koralROS/koralNode -width=${width:-640} -height=${height:-480} -maxFeatureCount=${maxFeatureCount:-50000} -fastThreshold=${fastThreshold:-40} -matchThreshold=${matchThreshold:-25} -scaleLevels=${scaleLevels:-8} -scaleFactor=${scaleFactor:-1.2} -sense1Topic=${sense1Topic:-imageL} -sense2Topic=${sense2Topic:-imageR}