FROM nvidia/cuda:10.2-devel-ubuntu16.04

# Dockerfile adapted from https://qiita.com/kndt84/items/9524b1ab3c4df6de30b8

ENV DEBIAN_FRONTEND noninteractive

ARG OPENCV_VERSION='3.4.9'
ARG GPU_ARCH='7.5'

WORKDIR /opt

# Build tools
RUN apt update && \
    apt install -y \
    sudo \
    tzdata \
    git \
    cmake \
    cmake-curses-gui \
    wget \
    unzip \
    build-essential

# Media I/O:
RUN apt install -y \
    zlib1g-dev \
    libjpeg-dev \
    libwebp-dev \
    libpng-dev \
    libtiff5-dev \
    libopenexr-dev \
    libgdal-dev \
    libgtk2.0-dev

# Video I/O:
RUN apt install -y \
    libdc1394-22-dev \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
    libtheora-dev \
    libvorbis-dev \
    libxvidcore-dev \
    libx264-dev \
    yasm \
    libopencore-amrnb-dev \
    libopencore-amrwb-dev \
    libv4l-dev \
    libxine2-dev \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev

# Parallelism and linear algebra libraries:
RUN apt install -y \
    libtbb-dev \
    libeigen3-dev

# Python:
RUN apt install -y \
    python3-dev \
    python3-tk \
    python3-numpy

# Build OpenCV
RUN wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip && \
    unzip ${OPENCV_VERSION}.zip && rm ${OPENCV_VERSION}.zip && \
    mv opencv-${OPENCV_VERSION} OpenCV && \
    cd OpenCV && \
    mkdir build && \
    cd build && \
    cmake \
    -D WITH_TBB=ON \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D BUILD_EXAMPLES=ON \
    -D WITH_FFMPEG=ON \
    -D WITH_V4L=ON \
    -D WITH_OPENGL=ON \
    -D WITH_CUDA=ON \
    -D CUDA_ARCH_BIN=${GPU_ARCH} \
    -D CUDA_ARCH_PTX=${GPU_ARCH} \
    -D WITH_CUBLAS=ON \
    -D WITH_CUFFT=ON \
    -D WITH_EIGEN=ON \
    -D EIGEN_INCLUDE_PATH=/usr/include/eigen3 \
    -D CUDA_CUDA_LIBRARY=/usr/local/cuda/lib64/stubs/libcuda.so \
    .. && \
    make all -j$(nproc) && \
    make install

#ROS
RUN apt-get install -y lsb-core \
    && sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' \
    && apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 \
    && apt-get update \
    && apt-get install -y ros-kinetic-desktop-full \
    && echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc \
    # && source ~/.bashrc \
    && apt install -y python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential \
    && apt install python-rosdep \
    && rosdep init \
    && rosdep update 

# KORAL-ROS
RUN . /opt/ros/kinetic/setup.sh \
    && git clone --recurse-submodules https://bitbucket.org/dammian_miller/koral-ros.git \
    && cd koral-ros \
    && mkdir build && cd build \
    && cmake .. \
    && make -j$(nproc)

# GCC7
RUN apt-get install -y software-properties-common \
    && add-apt-repository ppa:ubuntu-toolchain-r/test \
    && apt update \
    && apt install g++-7 -y \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 \
    --slave /usr/bin/g++ g++ /usr/bin/g++-7 \
    && update-alternatives --config gcc

# VCPKG
RUN git clone https://github.com/Microsoft/vcpkg.git \
    && cd vcpkg \
    && ./bootstrap-vcpkg.sh \
    && ./vcpkg integrate install \
    && ln -s /opt/vcpkg/vcpkg /usr/bin/vcpkg

# XXHASH
RUN vcpkg install xxhash


# PostgreSQL 12 Headers
RUN apt install -y postgresql-common \
    && yes | sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh \
    && apt install -y --allow-unauthenticated libpqxx-dev libpq-dev postgresql-server-dev-all


# CMAKE 3.17
RUN wget https://github.com/Kitware/CMake/releases/download/v3.17.0/cmake-3.17.0.tar.gz \
    && tar xvfz cmake-3.17.0.tar.gz \
    && cd cmake-3.17.0 \
    && ./configure \
    && make -j$(nproc) \
    && make install

ENV CXXFLAGS -I/opt/vcpkg/installed/x64-linux/include -std=c++11 -DPQXX_HIDE_EXP_OPTIONAL

WORKDIR /opt/koral-ros

# # libpq && libpqxx
# RUN apt-get install bison flex -y \
#     && vcpkg install libpq libpqxx

# # taoPQ
# RUN git clone --recurse-submodules https://github.com/taocpp/taopq.git \
#     && cd taopq \
#     && mkdir build \
#     && cd build \
#     && cmake .. \
#     && make -j$(nproc)

# # CONAN
# RUN wget https://dl.bintray.com/conan/installers/conan-ubuntu-64_1_24_0.deb \
    # && apt install -y ./conan-ubuntu-64_1_24_0.deb 

